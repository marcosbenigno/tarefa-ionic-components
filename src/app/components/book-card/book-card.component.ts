import { Component, Input, OnInit } from '@angular/core';

import {
	NavController,
	ToastController
} from '@ionic/angular';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss'],
})
export class BookCardComponent implements OnInit {
  @Input() books: any;
  rate: number;

  constructor(public toastController: ToastController) { }

  async successToast(event: any) {
		const starInput = event.target.getAttribute("data-star");
		this.rate = starInput;
		let textSupporter = (starInput == 1) ? "estrela" : "estrelas";
		const toast = await this.toastController.create({
			message: `Sua avaliação de ${starInput} ${textSupporter} foi salva!`,
			duration: 2000
		});
		toast.present();
	}

  ngOnInit() {}

}
