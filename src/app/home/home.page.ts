import { Component } from '@angular/core';

import {
	Router
} from '@angular/router';


class Book {
  title: string;
  resume: string;
  price: number;
  author: string;
  cover: string;
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private router: Router) {}
  books: Book[];

	openPage() {
		this.router.navigate(['/book']);
	}

  ngOnInit() {
    this.books = [{
      title: "Harry Potter e a Ordem da Fênix",
      resume: "Harry não é mais um garoto. Aos 15 anos...",
      price: 17.86,
      author: "J.K. ROWLLING",
      cover: "book_1.jpg"

    }];

  }
}
